<?php

require_once 'connect.php';


$sql = "CREATE TABLE users
      (id       SERIAL PRIMARY KEY,
      name      TEXT    NOT NULL,
      age      TEXT    NOT NULL,
      country   TEXT     NOT NULL
      )";

$result = pg_query($dbconn, $sql);
if(!$result){
  echo pg_last_error($dbconn);
} else {
  echo "Table created successfully";
}

// Close the connection
pg_close($dbconn);