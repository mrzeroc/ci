<?php
class mbuku extends CI_Model{
	public function __construct()
	{
		$this->load->database();
	}
   function selectAll()
   {
		$this->db->order_by("kategori","desc"); 
		return $this->db->get('buku')->result();
   }
   public function delete($id)
   {
   	$this->db->delete('buku', array('id_buku'=>$id));
   }
}
?>