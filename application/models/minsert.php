<?php
class Minsert extends CI_Model {
	public function __construct()
	{
		$this->load->database();
	} 
 
    function tambah() {
        $judul = $this->input->post('judul');
        $pengarang  = $this->input->post('pengarang');
        $kategori = $this->input->post('kategori');
        $data = array (
            'judul' => $judul,
            'pengarang'  => $pengarang,
            'kategori'=> $kategori,
        );  
        $this->db->insert('buku',$data);
    }
}
?>