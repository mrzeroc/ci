<?php
class Mymodel extends CI_Model {
	public function __construct()
	{
    $this->load->database();
    $this->load->model('mymodel');
    $this->load->helper('url_helper');
    $this->load->helper(array('url','form'));
    $this->load->library('session');
	} 
 
    public function daftar() {
        $name = $this->input->post('name');
        $age  = $this->input->post('age');
        $country = $this->input->post('country');
        $data = array (
            'name' =>  $name,
            'age'  =>  $age,
            'country'=>  $country
        );
        $this->db->insert('users',$data);
        redirect('register');
    }
 
    public function ceklogin($namatable, $where) {
        return $this->db->get_where($namatable, $where);
    }
}
?>