<?php
class cbuku extends CI_Controller{
function __construct(){
	parent::__construct();
	$this->load->model('mbuku');
	$this->load->helper('url_helper');
}
function index()
{
	$data['title']='ini contoh untuk menampilkan data';
	$data['buku']=$this->mbuku->selectAll();
	$this->load->view('v_buku',$data);
	}
public function delete($id)
{
	$this->mbuku->delete($id);
	redirect('list');
}
}
?>