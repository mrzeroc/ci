<?php
Class Insert_buku extends CI_Controller {	
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->model('minsert');
		$this->load->helper(array('url','form'));
	}
	function index() {
		$this->load->view('v_insert');
	}
	
	function tambahdata() {
		if($this->input->post('submit')){
			$this->minsert->tambah();
		}
		$this->load->view('v_insert');
	}
}
?>