<?php
class Controler extends CI_Controller{
function __construct(){
	parent::__construct();
	$this->load->database();
	$this->load->model('mymodel');
	$this->load->helper('url_helper');
	$this->load->helper(array('url','form'));
	$this->load->library('session');
}
	function index()
	{
		$data['judul']="Home";
		$this->load->view('new/home',$data);
	}

	public function delete($id)
	{
		$this->mbuku->delete($id);
		redirect('list');
	}
	
	public function proses_daftar()
	{
		if($this->input->post('submit'))
		{
			$this->mymodel->daftar();
		}
		$this->load->view('new/home');
	}
	
	public function daftar()
	{
		$this->load->view('new/register');
		$this->load->view('new/home');
	}
	
	public function proses_login()
	{
		$name = $this->input->post('name');
		$age = $this->input->post('age');
		$where = array(
				'name' => $name,
				'age' => $age
		);
		$cek_exist = $this->mymodel->ceklogin("users",$where)->num_rows();
		if ($cek_exist > 0) {
			$data_sesi = array(
				'nama' => $name,
				'status' => TRUE
			);
			$this->session->set_userdata($data_sesi);
			echo "loginberhasil";
			redirect('home');
		}else{
			echo "username salah";
		}
		$this->load->view('new/home');
	}
	
	public function login()
	{
		$this->load->view('new/login');
		$this->load->view('new/home');
	}
	
	public function logout()
	{
		$this->session->sess_destroy();
		redirect('home');
	}
}
?>